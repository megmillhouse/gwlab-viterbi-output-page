from email import header
import sys
import numpy as np
import matplotlib.pyplot as plt
# matplotlib.use('Agg')
import glob
import csv

# ------------------------------------------------------
#   make_summary_page.py
#     Uses viterbi output files to make a summary page
#
#     To run: python make_summary_page.py path_to_viterbi_files
# ------------------------------------------------------



def parse_outfile(topdir):
    '''
    Reads in the output file from slurm to grab command line
    input: directory where .out file lives
    output: tblock (seconds, int), start time (Hz, int), log-likelihood threshold (float)
    '''
    output = glob.glob(topdir+"/*viterbi.out")[0] # Grab the command line
    # print(output)
    fp = open(output, 'r')
    lines = fp.readlines()
    cmdline = lines[0].split('--')
    for opt in cmdline:
        if opt.split('=')[0] == 'tblock':
            tblock = int(opt.split('=')[-1])
        if opt.split('=')[0] == 'start_time':
            start_time = int(opt.split('=')[-1])
        if opt.split('=')[0] == 'llthreshold':
            logL_threshold = float(opt.split('=')[-1])
    return tblock, start_time, logL_threshold

def parse_results_file(topdir):
    '''
    Read in results of top paths returned by viterbi 
    '''
    names = ['P','a0','Tp','logL','score','f']
    data = np.genfromtxt(topdir+'/results_a0_phase_loglikes_scores.dat',names=names)
    return(data)

def get_path(tblock, start_time, topdir):
    '''
    Returns time and frequency steps of the viterbi path
    '''
    f_path = np.genfromtxt(topdir+'/results_path.dat')
    Nstep = len(f_path)
    t_path = np.zeros(Nstep)
    for i in range(0,Nstep):
        t_path[i] = start_time+i*tblock
    return(t_path,f_path)


# ---- Main -----

try:
    topdir = sys.argv[1]
except:
    print('Error: need to supply path to Viterbi job files')
    sys.exit()

print('Reading data from %s' % topdir)

# -- Read in data
tdrift, start_time, logL_threshold = parse_outfile(topdir)
#print(tdrift, start_time, logL_threshold)
param_results = parse_results_file(topdir)

# -- save table as csv
header = ['logL','score','f','a0','Tp','P']
outcsv = open('table_in_csv.csv','w')
writer = csv.writer(outcsv)
writer.writerow(header)
writer.writerows(param_results[header]) # Make sure it's in the same order as table in html
outcsv.close()

# -- Plot best viterbi path
t,f = get_path(tdrift, start_time, topdir)
plt.plot(t,f,'.-')
plt.xlabel('Time (seconds)',fontsize=16)
plt.ylabel('Frequency (Hz)', fontsize=16)
# plt.title('Viterbi path of top candidate',fontsize=18)
plt.savefig('best_viterbi_path.png',bbox_inches='tight')

# -- Make web page
html_page = open('summary_page.html','w')
html_str = ''
html_str += '<!DOCTYPE html>\n\
<html>\n\
<body>\n\n'

html_str += '<h1>Job Summary</h1>\n\n'

# Make table
html_str += '<h2>Table of most significant candidates</h2>\n'
html_str += '<p>Bold entries indicate candidates above the threshold logL=%.6g</p>\n\n' % (logL_threshold)

html_str += '<table>\n\
    <tr>\n\
      <th>log-likelihood</th>\n\
      <th>Score</th>\n\
      <th>End frequency (Hz)</th>\n\
      <th>a0</th>\n\
      <th>T_p</th>\n\
      <th>P</th>\n\
    </tr>\n'

for ii in param_results:
    if ii['logL'] >= logL_threshold:
        html_str += '    <tr style="font-weight:bold">\n'
    else:
        html_str += '    <tr>\n'
    html_str += '      <td>%.5g</td>\n' % (ii['logL'])
    html_str += '      <td>%.3g</td>\n' % (ii['score'])
    html_str += '      <td>%.9g</td>\n' % (ii['f'])
    html_str += '      <td>%.6g</td>\n' % (ii['a0'])
    html_str += '      <td>%.9g</td>\n' % (ii['Tp'])
    html_str += '      <td>%.6g</td>\n' % (ii['P'])
    html_str += '    </tr>\n'
html_str += '</table>\n\n'

# Put in plot
html_str += '<h2>Viterbi path of most significant candidate</h2>\n'
html_str += '<img src="best_viterbi_path.png">\n'

html_str += '</body>\n\
</html>\n'

html_page.write(html_str)
html_page.close()

# -- finish
print("Done!")

